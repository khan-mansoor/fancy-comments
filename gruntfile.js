module.exports = function (grunt) {
    grunt.initConfig({

        concat : {
          options: {
              separator: '\n\n//------------------------------------\n',
              banner: '\n\n//----------------------------------------\n'
          },
          dist: {
              src: ['components/templates/hb_tmpls.js', 'components/scripts/*.js'],
              dest: 'builds/development/js/script.js'
          },
          prod: {
              src: ['components/templates/*.hbs', 'components/scripts/*.js'],
              dest: 'builds/production/js/script.js'
          }
        }, // concat
        bower_concat: {
          all: {
              dest: {
                  js: 'builds/development/js/_bower.js',
                  css: 'builds/development/css/_bower.css'
              }
          }
        }, //bower_concat
        sass: {
          dist: {
              options: {
                  style: 'expanded' //compressed
              },
              files: [{
                  src: 'components/sass/style.scss',
                  dest: 'builds/development/css/style.css'
              }]
          }
        }, // sass
        compass: {                  // Task
          dist: {                   // Target
              options: {              // Target options
                  sassDir: 'sass',
                  cssDir: 'css',
                  environment: 'production'
              }
          },
          dev: {                    // Another target
              options: {
                  sassDir: 'components/sass',
                  cssDir: 'builds/development/css'
              }
          }
        },
        handlebars: {
            compile: {
                options: {
                    namespace: 'JST'
                },
                files: {
                    'components/templates/hb_tmpls.js': 'components/templates/*.hbs'
                }
            }
        },
        wiredep: {
            task: {
                src: 'builds/development/**/*.html'
            }
        }, //wiredep
        connect: {
          server: {
              options: {
                  hostname: 'localhost',
                  port: 3000,
                  base: 'builds/development',
                  livereload: true
              }
          }
        }, //connect
        watch: {
            options: {
              spawn: false,
                livereload: true
            },
            scripts: {
                files: [
                    'builds/development/**/*.html',
                    'components/scripts/**/*.js',
                    'components/sass/**/*.scss'
                ],
                tasks: ['concat', 'handlebars']
            }
        } // watch

    }); // initConfig

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-handlebars');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-wiredep');
    grunt.loadNpmTasks('grunt-bower-concat');
    grunt.registerTask('default', ['wiredep', 'compass', 'bower_concat', 'handlebars', 'concat:dist', 'connect', 'watch'])

}; //wrapper function