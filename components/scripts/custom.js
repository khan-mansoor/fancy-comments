"use strict";

var debug = console.log,
    source   = $("#commentsTmpl").html(),
    template = Handlebars.compile(source),
    searchKey = null;

Handlebars.registerHelper('json', function(context) {
    return JSON.stringify(context);
});

//dateFormat Handlebars helper is imported from https://gist.github.com/stephentcannon/3409103
//used here with modifications
Handlebars.registerHelper('dateFormat', function(context) {

    var then = moment.utc(new Date(context)),
        now = moment.utc(getCurrentTime()),
        diff = then.diff(now);

    if (window.moment) {
        return moment.duration(diff).humanize(true); //had to remove Date(context)
    }else{
        return context;   //  moment plugin not available. return data as is.
    }

});

//Get the timestamp from the DOM element and process it to update the time
function updateTime(duration) {

    $('#commentslist').find('.date').each(function (e, k) {
        var timestamp = parseInt($(k).data('timestamp')),
            then = moment.utc(new Date(timestamp-(1000*duration))),
            now = moment.utc(getCurrentTime()),
            diff = then.diff(now)
        ;

        $(k).empty().append(moment.duration(diff).humanize(true));
        $(k).data('timestamp', timestamp-(1000*duration));
        debug("Time updated", timestamp);
    });

} // updateTime

//Get the comments from the datasource via AJAX/API call
function loadComments() {

    $.ajax({
        type: 'GET',
        url: 'data/json-comments-ds.json',
        success: function (data) {
            loadTmpl(data);
        }
    });

} // loadComments

//Load handlebars template to display comments on the page
function loadTmpl(comments) {
    $('#commentslist').empty().append(template({'comments': comments}));
    showSearch();
    setUpdateInterval(60);
} // loadTmpl

//set an  interval to update time on the page without reloading page
function setUpdateInterval(duration) {
    setInterval(function () {
        updateTime(duration);
    }, 1000*duration);
} // setUpdateInterval

//Load comments for the current child object
function loadMoreComments(element, obj) {
    // $(element).hide();
    $(element).next('.children').empty().append(template({'comments': obj.children}));
    if(searchKey != null)
        markText();
} // loadMoreComments

function highlightComments(event) {
    var target = event.target,
        text = $(event).val();

    debug(text);
}
//Highlight the text as the user types in the input box
var markText = function(ele) {

    if(searchKey == null || typeof ele != 'undefined')
        searchKey = $(ele).val();

    var keyword = searchKey,
        instance = new Mark(document.querySelectorAll(".comment"));

    instance.unmark(keyword);
    instance.mark(keyword, {
        "separateWordSearch": false
    });
};

//Show search box
function showSearch(){
    $('#searchbox').removeClass('hide');
}
